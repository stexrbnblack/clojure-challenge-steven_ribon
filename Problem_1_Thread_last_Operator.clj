(ns prueba.Problem_1-Thread_last_Operator)

;; Given the invoice defined in invoice.edn in this repo, use the thread-last ->>
;; operator to find all invoice items that satisfy the given conditions. Please write
;; a function that receives an invoice as an argument and returns all items that satisfy
;; the conditions described below.

;; Invoice Item Conditions
;; * At least have one item that has :iva 19%
;; * At least one item has retention :ret_fuente 1%
;; * Every item must satisfy EXACTLY one of the above two conditions. This means that an item
;; cannot have BOTH :iva 19% and retention :ret_fuente 1%

(def invoice (clojure.edn/read-string (slurp "invoice.edn")))

(let [items (filter (fn [item] (or (and (some #(= (:tax/rate %) 19) (:taxable/taxes item)) (not (some #(= (:retention/rate %) 1) (:retentionable/retentions item))))
                                   (and (not (some #(= (:tax/rate %) 19) (:taxable/taxes item))) (some #(= (:retention/rate %) 1) (:retentionable/retentions item)))))
                    (:invoice/items invoice))
      id-sku (map (fn [item] [ (:invoice-item/id item) (:invoice-item/sku item)]) items)]
  (println id-sku))
