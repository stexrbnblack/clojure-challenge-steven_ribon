(ns prueba.Problem_2-Core_Generating_Functions
  (:require [clojure.data.json :as json]))

;;Given the invoice defined in invoice.json found in this repo, generate
;; an invoice that passes the spec ::invoice defined in invoice-spec.clj.
;; Write a function that as an argument receives a file name (a JSON file
;; name in this case) and returns a clojure map such that

(def data (slurp "invoice.json"))
(def json-data (json/read-str data))

(if (s/valid? ::invoice json-data)
  (let [items (get-in json-data [:invoice :items])
        customer (get-in json-data [:invoice :customer])]
    (println "Invoice items: " items)
    (println "Customer information: " customer)))
