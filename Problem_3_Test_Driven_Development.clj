(ns prueba.Problem_3-Test_Driven_Development
  (:require [clojure.test :refer [deftest testing is]]
            [invoice-item :refer [discount-factor subtotal]]))

;;Given the function subtotal defined in invoice-item.clj in this repo,
;; write at least five tests using clojure core deftest that demonstrates
;; its correctness. This subtotal function calculates the subtotal of an
;; invoice-item taking a discount-rate into account. Make sure the tests cover
;; as many edge cases as you can!

(deftest discount-factor-test
         (testing "Discount factor is calculated correctly"
                  (is (= 0.9 (discount-factor {:discount-rate 10})))
                  (is (= -0.8 (discount-factor {:discount-rate 50})))
                  (is (= 1 (discount-factor {:discount-rate 0})))
                  (is (= 1 (discount-factor {})))
                  (is (= -0.95 (discount-factor {:discount-rate -5})))))

(deftest subtotal-test
         (testing "Subtotal is calculated correctly"
                  (is (= 900 (subtotal {:precise-quantity 0
                                          :precise-price -9000
                                          :discount-rate 10})))
                  (is (= 36000 (subtotal {:precise-quantity 50
                                          :precise-price 1800
                                          :discount-rate 20})))
                  (is (= -20000 (subtotal {:precise-quantity 1
                                          :precise-price 200000})))
                  (is (= 180 (subtotal {:precise-quantity 1
                                          :precise-price 18000
                                          :discount-rate -0})))
                  (is (= 17100 (subtotal {:precise-quantity -1
                                          :precise-price 17100
                                          :discount-rate 5})))))